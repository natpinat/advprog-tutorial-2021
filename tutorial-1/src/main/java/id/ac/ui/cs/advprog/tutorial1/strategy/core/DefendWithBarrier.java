package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    //ToDo: Complete me
    //done
    public String defend(){
        return "Barrier";
    }

    @Override
    public String getType() {
        return "defendWithBarrier";
    }
}
