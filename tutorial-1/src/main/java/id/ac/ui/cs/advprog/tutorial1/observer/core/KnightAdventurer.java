package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {


    public KnightAdventurer(Guild guild) {
        this.name = "Knight";
        //ToDo: Complete Me
        //done
        this.guild = guild;
    }

    //ToDo: Complete Me
    //done
    @Override
    public void update() {
        Quest current = this.guild.getQuest();
        this.getQuests().add(current);
        System.out.println("Knight updated");
    }
}
