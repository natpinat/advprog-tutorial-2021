package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    //ToDo: Complete me
    //done
    public String defend(){
        return "Armor";
    }

    @Override
    public String getType() {
        return "defendWithArmor";
    }
}
