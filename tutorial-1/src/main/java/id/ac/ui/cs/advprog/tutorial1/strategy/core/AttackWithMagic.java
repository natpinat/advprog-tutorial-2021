package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    //ToDo: Complete me
    //done
    public String attack(){
        return "Magic";
    }

    @Override
    public String getType() {
        return "attackWithMagic";
    }
}
