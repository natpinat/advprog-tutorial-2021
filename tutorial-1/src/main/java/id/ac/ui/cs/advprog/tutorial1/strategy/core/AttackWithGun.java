package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    //ToDo: Complete me
    //done
    public String attack(){
        return "Gun";
    }

    @Override
    public String getType() {
        return "attackWithGun";
    }
}
