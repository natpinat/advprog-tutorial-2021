package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {


    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        //ToDo: Complete Me
        //done
        this.guild = guild;
    }

    //ToDo: Complete Me
    //done
    @Override
    public void update() {
        Quest current = this.guild.getQuest();
        if (this.guild.getQuestType().equals("D")){
            this.getQuests().add(current);
        }
        else if (this.guild.getQuestType().equals("E")){
            this.getQuests().add(current);
        }
        else {
            return;
        }
        System.out.println("Mystic updated");
    }

}
