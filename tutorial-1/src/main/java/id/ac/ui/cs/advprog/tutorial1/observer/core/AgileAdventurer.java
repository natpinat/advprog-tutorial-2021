package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {


    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        //ToDo: Complete Me
        //done
        this.guild = guild;
    }

    //ToDo: Complete Me
    //done
    @Override
    public void update() {
        Quest current = this.guild.getQuest();
        if (this.guild.getQuestType().equals("D")){
            this.getQuests().add(current);
        }
        else if (this.guild.getQuestType().equals("R")){
            this.getQuests().add(current);
        }
        else {
            return;
        }
        System.out.println("Agile updated");
    }

}
