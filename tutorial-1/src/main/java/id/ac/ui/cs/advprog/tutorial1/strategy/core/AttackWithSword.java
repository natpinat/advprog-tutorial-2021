package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
    //ToDo: Complete me
    //done
    public String attack(){
        return "Sword";
    }

    @Override
    public String getType() {
        return "attackWithSword";
    }
}
