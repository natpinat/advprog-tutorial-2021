package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {

    //ToDo: Complete me
    //done
    public MysticAdventurer(){
        //default
        setAttackBehavior(new AttackWithMagic());
        setDefenseBehavior(new DefendWithShield());
    }

    @Override
    public String getAlias() {
        return "mystic";
    }
}
