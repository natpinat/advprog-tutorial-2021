package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngridientsFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;

public class Menu{
    private String name;
    private Noodle noodle;
    private Meat meat;
    private Topping topping;
    private Flavor flavor;

    //To Do : Complete Me
    //Silahkan tambahkan parameter jika dibutuhkan
    public Menu(String name, IngridientsFactory cook){
        this.name = name;
        this.setNoodle(cook.createNoodle());
        this.setMeat(cook.createMeat());
        this.setTopping(cook.createTopping());
        this.setFlavor(cook.createFlavor());
    }

    private void setNoodle(Noodle noodle){
        this.noodle = noodle;
    }
    private void setMeat(Meat meat){
        this.meat = meat;
    }
    private void setTopping(Topping topping){
        this.topping = topping;
    }
    private void setFlavor(Flavor flavor){
        this.flavor = flavor;
    }

    public String getName() {
        return name;
    }

    public Noodle getNoodle() {
        return noodle;
    }

    public Meat getMeat() {
        return meat;
    }

    public Topping getTopping() {
        return topping;
    }

    public Flavor getFlavor() {
        return flavor;
    }
}