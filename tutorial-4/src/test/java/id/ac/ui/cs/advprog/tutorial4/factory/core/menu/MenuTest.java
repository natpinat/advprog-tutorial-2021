package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MenuTest {
    private Class<?> menuClass;

    @BeforeEach
    public void setup() throws Exception {
        menuClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu");
    }

    @Test
    public void testMenuIsAPublicClass() {
        int classModifiers = menuClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testMenuHasSetNoodlePublicMethod() throws Exception {
        Class<?>[] noodle = new Class[1];
        noodle[0] = Noodle.class;
        Method setNoodle = menuClass.getDeclaredMethod("setNoodle",noodle);
        int methodModifiers = setNoodle.getModifiers();

        assertTrue(Modifier.isPrivate(methodModifiers));
        assertEquals(1, setNoodle.getParameterCount());
    }

    @Test
    public void testMenuHasSetMeatPublicMethod() throws Exception {
        Class<?>[] meat = new Class[1];
        meat[0] = Meat.class;
        Method setMeat = menuClass.getDeclaredMethod("setMeat",meat);
        int methodModifiers = setMeat.getModifiers();

        assertTrue(Modifier.isPrivate(methodModifiers));
        assertEquals(1, setMeat.getParameterCount());
    }

    @Test
    public void testMenuHasSetToppingPublicMethod() throws Exception {
        Class<?>[] topping = new Class[1];
        topping[0] = Topping.class;
        Method setTopping = menuClass.getDeclaredMethod("setTopping",topping);
        int methodModifiers = setTopping.getModifiers();

        assertTrue(Modifier.isPrivate(methodModifiers));
        assertEquals(1, setTopping.getParameterCount());
    }
    @Test
    public void testMenuHasSetFlavorPublicMethod() throws Exception {
        Class<?>[] flavor = new Class[1];
        flavor[0] = Flavor.class;
        Method setFlavor = menuClass.getDeclaredMethod("setFlavor",flavor);
        int methodModifiers = setFlavor.getModifiers();

        assertTrue(Modifier.isPrivate(methodModifiers));
        assertEquals(1, setFlavor.getParameterCount());
    }

    @Test
    public void testMenuOverrideGetNoodleMethod() throws Exception {
        Method getNoodle = menuClass.getDeclaredMethod("getNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                getNoodle.getGenericReturnType().getTypeName());
        assertEquals(0,
                getNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(getNoodle.getModifiers()));
    }

    @Test
    public void testMenuOverrideGetMeatMethod() throws Exception {
        Method getMeat = menuClass.getDeclaredMethod("getMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                getMeat.getGenericReturnType().getTypeName());
        assertEquals(0,
                getMeat.getParameterCount());
        assertTrue(Modifier.isPublic(getMeat.getModifiers()));
    }

    @Test
    public void testMenuOverrideGetToppingMethod() throws Exception {
        Method getTopping = menuClass.getDeclaredMethod("getTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                getTopping.getGenericReturnType().getTypeName());
        assertEquals(0,
                getTopping.getParameterCount());
        assertTrue(Modifier.isPublic(getTopping.getModifiers()));
    }

    @Test
    public void testMenuOverrideGetFlavorMethod() throws Exception {
        Method getFlavor = menuClass.getDeclaredMethod("getFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                getFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                getFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(getFlavor.getModifiers()));
    }

    @Test
    public void testMenuOverrideGetNameMethod() throws Exception {
        Method getName = menuClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

}
