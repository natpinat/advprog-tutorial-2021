package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class MenuServiceTest {
    private Class<?> menuServiceClass;

    @InjectMocks
    MenuServiceImpl menuService = new MenuServiceImpl();

    @BeforeEach
    public void setup() throws Exception {
        menuServiceClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.factory.service.MenuServiceImpl");
    }

    @Test
    public void testMenuServiceHasCreateMenuMethod() throws Exception {
        Class<?>[] menu = new Class[2];
        menu[0] = String.class;
        menu[1] = String.class;
        Method createMenu = menuServiceClass.getDeclaredMethod("createMenu", menu);
        int methodModifiers = createMenu.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(2, createMenu.getParameterCount());
    }

    @Test
    public void testMenuServiceCreateMenuReturnCorrectMenuAmount() {
        //test createMenu
        Menu added = menuService.createMenu("soba","InuzumaRamen");
        assertEquals("soba", added.getName());

        Menu added1 = menuService.createMenu("soba","LiyuanSoba");
        assertEquals("soba", added1.getName());
        Menu added2 = menuService.createMenu("soba","MondoUdon");
        assertEquals("soba", added2.getName());
        Menu added3 = menuService.createMenu("soba","SnevnezhaShirataki");
        assertEquals("soba", added3.getName());

    }

    @Test
    public void testMenuServiceHasGetMenusMethod() throws Exception {
        Method getMenu = menuServiceClass.getDeclaredMethod("getMenus");
        int methodModifiers = getMenu.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));

        assertEquals("java.util.List<id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu>",
                getMenu.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMenuServiceHasInitRepoMethod() throws Exception {
        Method initRepo = menuServiceClass.getDeclaredMethod("initRepo");
        int methodModifiers = initRepo.getModifiers();
        assertTrue(Modifier.isPrivate(methodModifiers));
    }
}
