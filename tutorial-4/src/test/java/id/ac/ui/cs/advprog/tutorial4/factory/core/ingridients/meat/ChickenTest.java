package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ChickenTest {
    private Class<?> chickenClass;

    @BeforeEach
    public void setUp() throws Exception {
        chickenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken");
    }

    @Test
    public void testChickenIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(chickenClass.getModifiers()));
    }

    @Test
    public void testChickenImplementsFlavor() {
        Collection<Type> interfaces = Arrays.asList(chickenClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat")));
    }

    @Test
    public void testChickenOverrideGetDescriptionMethod() throws Exception {
        Method getDesciption = chickenClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDesciption.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDesciption.getParameterCount());
        assertTrue(Modifier.isPublic(getDesciption.getModifiers()));
    }

    @Test
    public void testChickenValueGetDescription(){
        Chicken chicken = new Chicken();
        String value = chicken.getDescription();
        assertEquals("Adding Wintervale Chicken Meat...",value);
    }

}
