package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;
import static org.junit.jupiter.api.Assertions.*;

public class InuzumaRamenTest {
    private Class<?> ramenClass;
    private InuzumaRamen ramen = new InuzumaRamen("ramen");

    @BeforeEach
    public void setUp() throws Exception {
        ramenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen");

    }

    @Test
    public void testInuzumaRamenIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(ramenClass.getModifiers()));
    }

    @Test
    public void testInuzumaRamenIsAMenu() {
        Class<?> parentClass = ramenClass.getSuperclass();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu",
                parentClass.getName());
    }

    @Test
    public void testInuzumaRamenGetter() throws Exception {
        Ramen a = new Ramen();
        Pork b = new Pork();
        BoiledEgg c = new BoiledEgg();
        Spicy d = new Spicy();
        assertEquals("ramen", ramen.getName());
        assertEquals(a.getDescription(), ramen.getNoodle().getDescription());
        assertEquals(b.getDescription(), ramen.getMeat().getDescription());
        assertEquals(c.getDescription(), ramen.getTopping().getDescription());
        assertEquals(d.getDescription(), ramen.getFlavor().getDescription());
    }

}