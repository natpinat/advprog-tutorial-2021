package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RamenTest {
    private Class<?> ramenClass;

    @BeforeEach
    public void setUp() throws Exception {
        ramenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen");
    }

    @Test
    public void testRamenIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(ramenClass.getModifiers()));
    }

    @Test
    public void testRamenImplementsFlavor() {
        Collection<Type> interfaces = Arrays.asList(ramenClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle")));
    }

    @Test
    public void testRamenOverrideGetDescriptionMethod() throws Exception {
        Method getDesciption = ramenClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDesciption.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDesciption.getParameterCount());
        assertTrue(Modifier.isPublic(getDesciption.getModifiers()));
    }

    @Test
    public void testRamenValueGetDescription(){
        Ramen ramen = new Ramen();
        String value = ramen.getDescription();
        assertEquals("Adding Inuzuma Ramen Noodles...",value);
    }

}
