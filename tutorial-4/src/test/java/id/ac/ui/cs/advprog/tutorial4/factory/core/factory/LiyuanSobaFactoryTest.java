package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LiyuanSobaFactoryTest {
    private Class<?> sobaClass;

    @BeforeEach
    public void setUp() throws Exception {
        sobaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.LiyuanSobaFactory");
    }

    @Test
    public void testLiyuanSobaIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(sobaClass.getModifiers()));
    }

    @Test
    public void testLiyuanSobaImplementsIngredientsFactory() {
        Collection<Type> interfaces = Arrays.asList(sobaClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngridientsFactory")));
    }

    @Test
    public void testLiyuanSobaOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = sobaClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0,
                createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testLiyuanSobaOverrideCreateMeatMethod() throws Exception {
        Method createMeat = sobaClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0,
                createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }

    @Test
    public void testLiyuanSobaOverrideCreateToppingMethod() throws Exception {
        Method createTopping = sobaClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0,
                createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }

    @Test
    public void testLiyuanSobaOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = sobaClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }

    @Test
    public void testLiyuanSobaValueCreateNoodle(){
        LiyuanSobaFactory soba = new LiyuanSobaFactory();
        Noodle value = soba.createNoodle();
        assertEquals("Adding Liyuan Soba Noodles...",value.getDescription());
    }
    @Test
    public void testLiyuanSobaValueCreateMeat(){
        LiyuanSobaFactory soba = new LiyuanSobaFactory();
        Meat value = soba.createMeat();
        assertEquals("Adding Maro Beef Meat...",value.getDescription());
    }
    @Test
    public void testLiyuanSobaValueCreateTopping(){
        LiyuanSobaFactory soba = new LiyuanSobaFactory();
        Topping value = soba.createTopping();
        assertEquals("Adding Shiitake Mushroom Topping...",value.getDescription());
    }

    @Test
    public void testLiyuanSobaValueCreateFlavor(){
        LiyuanSobaFactory soba = new LiyuanSobaFactory();
        Flavor value = soba.createFlavor();
        assertEquals("Adding a dash of Sweet Soy Sauce...",value.getDescription());
    }
}
