package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MushroomTest {
    private Class<?> mushroomClass;

    @BeforeEach
    public void setUp() throws Exception {
        mushroomClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom");
    }

    @Test
    public void testMushroomIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(mushroomClass.getModifiers()));
    }

    @Test
    public void testMushroomImplementsFlavor() {
        Collection<Type> interfaces = Arrays.asList(mushroomClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping")));
    }

    @Test
    public void testMushroomOverrideGetDescriptionMethod() throws Exception {
        Method getDesciption = mushroomClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDesciption.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDesciption.getParameterCount());
        assertTrue(Modifier.isPublic(getDesciption.getModifiers()));
    }

    @Test
    public void testMushroomValueGetDescription(){
        Mushroom mushroom = new Mushroom();
        String value = mushroom.getDescription();
        assertEquals("Adding Shiitake Mushroom Topping...",value);
    }
}
