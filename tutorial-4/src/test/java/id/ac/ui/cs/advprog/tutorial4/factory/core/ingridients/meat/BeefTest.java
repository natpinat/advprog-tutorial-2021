package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BeefTest {
    private Class<?> beefClass;

    @BeforeEach
    public void setUp() throws Exception {
        beefClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef");
    }

    @Test
    public void testBeefIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(beefClass.getModifiers()));
    }

    @Test
    public void testBeefImplementsFlavor() {
        Collection<Type> interfaces = Arrays.asList(beefClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat")));
    }

    @Test
    public void testBeefOverrideGetDescriptionMethod() throws Exception {
        Method getDesciption = beefClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDesciption.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDesciption.getParameterCount());
        assertTrue(Modifier.isPublic(getDesciption.getModifiers()));
    }

    @Test
    public void testBeefValueGetDescription(){
        Beef beef = new Beef();
        String value = beef.getDescription();
        assertEquals("Adding Maro Beef Meat...",value);
    }

}
