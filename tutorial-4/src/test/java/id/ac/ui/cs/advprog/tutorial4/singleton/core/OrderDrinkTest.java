package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class OrderDrinkTest {
    private Class<?> drinkClass;
    private OrderDrink drink;

    @BeforeEach
    public void setUp() throws Exception {
        drinkClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink");
    }

    @Test
    public void testOrderDrinkIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(drinkClass.getModifiers()));
    }

    @Test
    public void testOrderDrinkSetGetterDrink() throws Exception {
        drink = drink.getInstance();
        drink.setDrink("mango");
        assertEquals("mango", drink.getDrink());
    }


}
