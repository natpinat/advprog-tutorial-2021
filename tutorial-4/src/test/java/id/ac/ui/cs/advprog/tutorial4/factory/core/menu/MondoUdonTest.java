package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;
import static org.junit.jupiter.api.Assertions.*;

public class MondoUdonTest {
    private Class<?> udonClass;
    private MondoUdon udon = new MondoUdon("udon");

    @BeforeEach
    public void setUp() throws Exception {
        udonClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon");

    }

    @Test
    public void testMondoUdonIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(udonClass.getModifiers()));
    }

    @Test
    public void testMondoUdonIsAMenu() {
        Class<?> parentClass = udonClass.getSuperclass();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu",
                parentClass.getName());
    }

    @Test
    public void testMondoUdonGetter() throws Exception {
        Udon a = new Udon();
        Chicken b = new Chicken();
        Cheese c = new Cheese();
        Salty d = new Salty();
        assertEquals("udon", udon.getName());
        assertEquals(a.getDescription(), udon.getNoodle().getDescription());
        assertEquals(b.getDescription(), udon.getMeat().getDescription());
        assertEquals(c.getDescription(), udon.getTopping().getDescription());
        assertEquals(d.getDescription(), udon.getFlavor().getDescription());
    }

}