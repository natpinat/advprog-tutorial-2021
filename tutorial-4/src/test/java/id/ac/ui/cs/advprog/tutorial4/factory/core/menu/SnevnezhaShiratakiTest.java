package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;
import static org.junit.jupiter.api.Assertions.*;

public class SnevnezhaShiratakiTest {
    private Class<?> shiratakiClass;
    private SnevnezhaShirataki shirataki = new SnevnezhaShirataki("shirataki");

    @BeforeEach
    public void setUp() throws Exception {
        shiratakiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki");

    }

    @Test
    public void testSnevnezhaShiratakiIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(shiratakiClass.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiIsAMenu() {
        Class<?> parentClass = shiratakiClass.getSuperclass();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu",
                parentClass.getName());
    }

    @Test
    public void testSnevnezhaShiratakiGetter() throws Exception {
        Shirataki a = new Shirataki();
        Fish b = new Fish();
        Flower c = new Flower();
        Umami d = new Umami();
        assertEquals("shirataki", shirataki.getName());
        assertEquals(a.getDescription(), shirataki.getNoodle().getDescription());
        assertEquals(b.getDescription(), shirataki.getMeat().getDescription());
        assertEquals(c.getDescription(), shirataki.getTopping().getDescription());
        assertEquals(d.getDescription(), shirataki.getFlavor().getDescription());
    }

}