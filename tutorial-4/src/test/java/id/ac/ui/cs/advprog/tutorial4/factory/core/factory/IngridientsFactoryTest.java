package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class IngridientsFactoryTest {
    private Class<?> ingredientsClass;

    @BeforeEach
    public void setup() throws Exception {
        ingredientsClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngridientsFactory");
    }

    @Test
    public void testIngredientsfactoryIsAPublicInterface() {
        int classModifiers = ingredientsClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testIngredientsFactoryHasCreateNoodlePublicMethod() throws Exception {
        Method createNoodle = ingredientsClass.getDeclaredMethod("createNoodle");
        int methodModifiers = createNoodle.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(0, createNoodle.getParameterCount());
    }

    @Test
    public void testIngredientsFactoryHasCreateMeatPublicMethod() throws Exception {
        Method createMeat = ingredientsClass.getDeclaredMethod("createMeat");
        int methodModifiers = createMeat.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(0, createMeat.getParameterCount());
    }

    @Test
    public void testIngredientsFactoryHasCreateToppingPublicMethod() throws Exception {

        Method createTopping = ingredientsClass.getDeclaredMethod("createTopping");
        int methodModifiers = createTopping.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(0, createTopping.getParameterCount());
    }
    @Test
    public void testIngredientsFactoryHasCreateFlavorPublicMethod() throws Exception {
        Method createFlavor = ingredientsClass.getDeclaredMethod("createFlavor");
        int methodModifiers = createFlavor.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(0, createFlavor.getParameterCount());
    }

}
