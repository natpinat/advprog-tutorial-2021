package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class InuzumaRamenFactoryTest {
    private Class<?> ramenClass;

    @BeforeEach
    public void setUp() throws Exception {
        ramenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.InuzumaRamenFactory");
    }

    @Test
    public void testInuzumaRamenIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(ramenClass.getModifiers()));
    }

    @Test
    public void testInuzumaRamenImplementsIngredientsFactory() {
        Collection<Type> interfaces = Arrays.asList(ramenClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngridientsFactory")));
    }

    @Test
    public void testInuzumaRamenOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = ramenClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0,
                createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testInuzumaRamenOverrideCreateMeatMethod() throws Exception {
        Method createMeat = ramenClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0,
                createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }

    @Test
    public void testInuzumaRamenOverrideCreateToppingMethod() throws Exception {
        Method createTopping = ramenClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0,
                createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }

    @Test
    public void testInuzumaRamenOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = ramenClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }

    @Test
    public void testInuzumaRamenValueCreateNoodle(){
        InuzumaRamenFactory ramen = new InuzumaRamenFactory();
        Noodle value = ramen.createNoodle();
        assertEquals("Adding Inuzuma Ramen Noodles...",value.getDescription());
    }
    @Test
    public void testInuzumaRamenValueCreateMeat(){
        InuzumaRamenFactory ramen = new InuzumaRamenFactory();
        Meat value = ramen.createMeat();
        assertEquals("Adding Tian Xu Pork Meat...",value.getDescription());
    }
    @Test
    public void testInuzumaRamenValueCreateTopping(){
        InuzumaRamenFactory ramen = new InuzumaRamenFactory();
        Topping value = ramen.createTopping();
        assertEquals("Adding Guahuan Boiled Egg Topping",value.getDescription());
    }

    @Test
    public void testInuzumaRamenValueCreateFlavor(){
        InuzumaRamenFactory ramen = new InuzumaRamenFactory();
        Flavor value = ramen.createFlavor();
        assertEquals("Adding Liyuan Chili Powder...",value.getDescription());
    }
}
