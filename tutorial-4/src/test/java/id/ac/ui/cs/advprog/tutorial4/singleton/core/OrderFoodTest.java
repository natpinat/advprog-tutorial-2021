package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class OrderFoodTest {
    private Class<?> foodClass;
    private OrderFood food ;

    @BeforeEach
    public void setUp() throws Exception {
        foodClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood");
    }

    @Test
    public void testOrderFoodIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(foodClass.getModifiers()));
    }

    @Test
    public void testOrderFoodSetGetterFood() throws Exception {
        food = food.getInstance();
        food.setFood("mango");
        assertEquals("mango", food.getFood());
    }


}
