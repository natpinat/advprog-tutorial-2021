package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ShiratakiTest {
    private Class<?> shiratakiClass;

    @BeforeEach
    public void setUp() throws Exception {
        shiratakiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki");
    }

    @Test
    public void testShiratakiIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(shiratakiClass.getModifiers()));
    }

    @Test
    public void testShiratakiImplementsFlavor() {
        Collection<Type> interfaces = Arrays.asList(shiratakiClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle")));
    }

    @Test
    public void testShiratakiOverrideGetDescriptionMethod() throws Exception {
        Method getDesciption = shiratakiClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDesciption.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDesciption.getParameterCount());
        assertTrue(Modifier.isPublic(getDesciption.getModifiers()));
    }

    @Test
    public void testShiratakiValueGetDescription(){
        Shirataki shirataki = new Shirataki();
        String value = shirataki.getDescription();
        assertEquals("Adding Snevnezha Shirataki Noodles...",value);
    }

}
