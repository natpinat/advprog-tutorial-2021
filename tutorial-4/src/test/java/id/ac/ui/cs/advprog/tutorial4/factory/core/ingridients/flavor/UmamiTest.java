package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UmamiTest {
    private Class<?> umamiClass;

    @BeforeEach
    public void setUp() throws Exception {
        umamiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami");
    }

    @Test
    public void testUmamiIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(umamiClass.getModifiers()));
    }

    @Test
    public void testUmamiImplementsFlavor() {
        Collection<Type> interfaces = Arrays.asList(umamiClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor")));
    }

    @Test
    public void testUmamiOverrideGetDescriptionMethod() throws Exception {
        Method getDesciption = umamiClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDesciption.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDesciption.getParameterCount());
        assertTrue(Modifier.isPublic(getDesciption.getModifiers()));
    }

    @Test
    public void testUmamiValueGetDescription(){
        Umami umami = new Umami();
        String value = umami.getDescription();
        assertEquals("Adding WanPlus Specialty MSG flavoring...",value);
    }

}
