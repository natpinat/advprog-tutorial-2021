package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;
import static org.junit.jupiter.api.Assertions.*;

public class LiyuanSobaTest {
    private Class<?> sobaClass;
    private LiyuanSoba soba = new LiyuanSoba("soba");

    @BeforeEach
    public void setUp() throws Exception {
        sobaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba");

    }

    @Test
    public void testLiyuanSobaIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(sobaClass.getModifiers()));
    }

    @Test
    public void testLiyuanSobaIsAMenu() {
        Class<?> parentClass = sobaClass.getSuperclass();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu",
                parentClass.getName());
    }

    @Test
    public void testLiyuanSobaGetter() throws Exception {
        Soba a = new Soba();
        Beef b = new Beef();
        Mushroom c = new Mushroom();
        Sweet d = new Sweet();
        assertEquals("soba", soba.getName());
        assertEquals(a.getDescription(), soba.getNoodle().getDescription());
        assertEquals(b.getDescription(), soba.getMeat().getDescription());
        assertEquals(c.getDescription(), soba.getTopping().getDescription());
        assertEquals(d.getDescription(), soba.getFlavor().getDescription());
    }

}