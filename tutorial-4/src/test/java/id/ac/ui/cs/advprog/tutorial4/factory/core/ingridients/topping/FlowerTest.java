package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FlowerTest {
    private Class<?> flowerClass;

    @BeforeEach
    public void setUp() throws Exception {
        flowerClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower");
    }

    @Test
    public void testFlowerIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(flowerClass.getModifiers()));
    }

    @Test
    public void testFlowerImplementsFlavor() {
        Collection<Type> interfaces = Arrays.asList(flowerClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping")));
    }

    @Test
    public void testFlowerOverrideGetDescriptionMethod() throws Exception {
        Method getDesciption = flowerClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDesciption.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDesciption.getParameterCount());
        assertTrue(Modifier.isPublic(getDesciption.getModifiers()));
    }

    @Test
    public void testFlowerValueGetDescription(){
        Flower flower = new Flower();
        String value = flower.getDescription();
        assertEquals("Adding Xinqin Flower Topping...",value);
    }
}
