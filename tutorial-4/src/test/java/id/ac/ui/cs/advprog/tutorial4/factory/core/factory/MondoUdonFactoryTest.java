package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MondoUdonFactoryTest {
    private Class<?> udonClass;

    @BeforeEach
    public void setUp() throws Exception {
        udonClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MondoUdonFactory");
    }

    @Test
    public void testMondoUdonIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(udonClass.getModifiers()));
    }

    @Test
    public void testMondoUdonImplementsIngredientsFactory() {
        Collection<Type> interfaces = Arrays.asList(udonClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngridientsFactory")));
    }

    @Test
    public void testMondoUdonOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = udonClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0,
                createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testMondoUdonOverrideCreateMeatMethod() throws Exception {
        Method createMeat = udonClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0,
                createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }

    @Test
    public void testMondoUdonOverrideCreateToppingMethod() throws Exception {
        Method createTopping = udonClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0,
                createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }

    @Test
    public void testMondoUdonOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = udonClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }

    @Test
    public void testMondoUdonValueCreateNoodle(){
        MondoUdonFactory udon = new MondoUdonFactory();
        Noodle value = udon.createNoodle();
        assertEquals("Adding Mondo Udon Noodles...",value.getDescription());
    }
    @Test
    public void testMondoUdonValueCreateMeat(){
        MondoUdonFactory udon = new MondoUdonFactory();
        Meat value = udon.createMeat();
        assertEquals("Adding Wintervale Chicken Meat...",value.getDescription());
    }
    @Test
    public void testMondoUdonValueCreateTopping(){
        MondoUdonFactory udon = new MondoUdonFactory();
        Topping value = udon.createTopping();
        assertEquals("Adding Shredded Cheese Topping...",value.getDescription());
    }

    @Test
    public void testMondoUdonValueCreateFlavor(){
        MondoUdonFactory udon = new MondoUdonFactory();
        Flavor value = udon.createFlavor();
        assertEquals("Adding a pinch of salt...",value.getDescription());
    }
}