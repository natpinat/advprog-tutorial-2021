package id.ac.ui.cs.advprog.tutorial4.singleton;

import id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class OrderServiceTest {
    private Class<?> orderServiceClass;

    @InjectMocks
    OrderServiceImpl orderService = new OrderServiceImpl();

    @BeforeEach
    public void setup() throws Exception {
        orderServiceClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl");
    }

    @Test
    public void testOrderServiceHasOrderDrinkMethod() throws Exception {
        Class<?>[] order = new Class[1];
        order[0] = String.class;
        Method orderDrink = orderServiceClass.getDeclaredMethod("orderADrink", order);
        int methodModifiers = orderDrink.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, orderDrink.getParameterCount());
    }

    @Test
    public void testOrderServiceOrderADrinkAndGetter() {
        orderService.getDrink();
        orderService.orderADrink("test");
        assertEquals("test", orderService.getDrink().getDrink());
    }

    @Test
    public void testOrderServiceHasOrderFoodMethod() throws Exception {
        Class<?>[] order = new Class[1];
        order[0] = String.class;
        Method orderFood = orderServiceClass.getDeclaredMethod("orderAFood", order);
        int methodModifiers = orderFood.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, orderFood.getParameterCount());
    }

    @Test
    public void testOrderServiceOrderAFoodAndGetter() {
        orderService.getFood();
        orderService.orderAFood("test");
        assertEquals("test", orderService.getFood().getFood());
    }
}