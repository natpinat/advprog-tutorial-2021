package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SnevnezhaShiratakiFactoryTest {
    private Class<?> shiratakiClass;

    @BeforeEach
    public void setUp() throws Exception {
        shiratakiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.SnevnezhaShiratakiFactory");
    }

    @Test
    public void testSnevnezhaShiratakiIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(shiratakiClass.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiImplementsIngredientsFactory() {
        Collection<Type> interfaces = Arrays.asList(shiratakiClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngridientsFactory")));
    }

    @Test
    public void testSnevnezhaShiratakiOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = shiratakiClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0,
                createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiOverrideCreateMeatMethod() throws Exception {
        Method createMeat = shiratakiClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0,
                createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiOverrideCreateToppingMethod() throws Exception {
        Method createTopping = shiratakiClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0,
                createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = shiratakiClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiValueCreateNoodle(){
        SnevnezhaShiratakiFactory shirataki = new SnevnezhaShiratakiFactory();
        Noodle value = shirataki.createNoodle();
        assertEquals("Adding Snevnezha Shirataki Noodles...",value.getDescription());
    }
    @Test
    public void testSnevnezhaShiratakiValueCreateMeat(){
        SnevnezhaShiratakiFactory shirataki = new SnevnezhaShiratakiFactory();
        Meat value = shirataki.createMeat();
        assertEquals("Adding Zhangyun Salmon Fish Meat...",value.getDescription());
    }
    @Test
    public void testSnevnezhaShiratakiValueCreateTopping(){
        SnevnezhaShiratakiFactory shirataki = new SnevnezhaShiratakiFactory();
        Topping value = shirataki.createTopping();
        assertEquals("Adding Xinqin Flower Topping...",value.getDescription());
    }

    @Test
    public void testSnevnezhaShiratakiValueCreateFlavor(){
        SnevnezhaShiratakiFactory shirataki = new SnevnezhaShiratakiFactory();
        Flavor value = shirataki.createFlavor();
        assertEquals("Adding WanPlus Specialty MSG flavoring...",value.getDescription());
    }
}