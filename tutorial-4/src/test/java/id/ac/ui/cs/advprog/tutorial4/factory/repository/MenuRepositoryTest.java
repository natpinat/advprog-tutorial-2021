package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;


import static org.assertj.core.api.Assertions.assertThat;

@Repository
public class MenuRepositoryTest {
    private MenuRepository menuRepository;
    private List<Menu> menus = new ArrayList<>();

    private Menu sampleMenu;

    @BeforeEach
    public void setUp() {
        menuRepository = new MenuRepository();
        sampleMenu = new InuzumaRamen("menu");
        menuRepository.add(sampleMenu);
        menus.add(sampleMenu);
    }

    @Test
    public void whenMenuRepoGetMenusItShouldReturnMenuList() {
        List<Menu> acquiredMenus = menuRepository.getMenus();
        assertThat(acquiredMenus).isEqualTo(menus);
    }

    @Test
    public void whenMenuRepoAddItShouldAddMenu() {
        Menu newMenu = new InuzumaRamen("ramen");
        Menu acquiredMenu = menuRepository.add(newMenu);
        assertThat(acquiredMenu.getName()).isEqualTo(newMenu.getName());
    }
}
