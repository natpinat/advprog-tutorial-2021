package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BoiledEggTest {
    private Class<?> boiledEggClass;

    @BeforeEach
    public void setUp() throws Exception {
        boiledEggClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg");
    }

    @Test
    public void testBoiledEggIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(boiledEggClass.getModifiers()));
    }

    @Test
    public void testBoiledEggImplementsFlavor() {
        Collection<Type> interfaces = Arrays.asList(boiledEggClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping")));
    }

    @Test
    public void testBoiledEggOverrideGetDescriptionMethod() throws Exception {
        Method getDesciption = boiledEggClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDesciption.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDesciption.getParameterCount());
        assertTrue(Modifier.isPublic(getDesciption.getModifiers()));
    }

    @Test
    public void testBoiledEggValueGetDescription(){
        BoiledEgg boiledEgg = new BoiledEgg();
        String value = boiledEgg.getDescription();
        assertEquals("Adding Guahuan Boiled Egg Topping",value);
    }

}
