#Perbedaan lazy instantiation dan eager instantiation

Pada lazy instantiation, class akan diinstantiasi ketika class tersebut dipanggil atau dibutuhkan.
Berbeda halnya dengan eager instantiation, walaupun class tersebut dibutuhkan atau tidak, class akan diinstantiasi
ketika programnya dijalankan tetapi sebelum object yang membutuhkan class tersebut dipanggil.

Keuntungan lazy instantiation tentunya tidak memberatkan memory space dan running time ketika object tersebut
hanya digunakan di sebagian saja. Kerugiannya adalah bila lazy instantiation digunakan pada object yang hampir setiap
bagian dari project menggunakan object tersebut sehingga perlu membutuhkan waktu untuk menunggu class tersebut diload
ketika memanggil suatu method.

Keuntungan eager instantiation adalah ketika object tersebut sering digunakan maka tidak perlu menunggu lagi bila suatu
method yang menggunakan class tersebut dipanggil. Sedangkan kerugiannya adalah dalam segi memory space karena sebelum
program dijalankan semua class yang mengimplementasikan eager instantiation harus diinstantiasi dulu sehingga mengisi 
memory space terutama bila class tidak terlalu sering digunakan.