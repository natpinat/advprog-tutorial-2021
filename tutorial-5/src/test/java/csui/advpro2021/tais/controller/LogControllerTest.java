package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.service.LogServiceImpl;
import csui.advpro2021.tais.service.MahasiswaService;
import csui.advpro2021.tais.service.MahasiswaServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = LogController.class)
class LogControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogServiceImpl logService;

    @MockBean
    private MahasiswaRepository mahasiswaRepository;


    private Log log;
    private Mahasiswa mahasiswa;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa();
        mahasiswa.setNpm("1234");
        log = new Log();
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime start = LocalDateTime.parse("2020-03-04 12:00:00",df);
        LocalDateTime end = LocalDateTime.parse("2020-03-04 14:00:00",df);
        log.setStartTime(start);
        log.setEndTime(end);
        log.setDescription("hmmmmmm");
        log.setKodeLog(1);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testControllerGetListLog() throws Exception {
        Iterable<Log> listLog = Arrays.asList(log);
        when(logService.getListLog()).thenReturn(listLog);

        mvc.perform(get("/log").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].kodeLog").value(log.getKodeLog()));
    }

    @Test
    void testControllerCreateLog() throws Exception {
        mahasiswa.setNpm("1234567");
        mahasiswaRepository.save(mahasiswa);
        when(logService.createLog(log,mahasiswa)).thenReturn(log);

        mvc.perform(post("/log")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(log)));
    }

    @Test
    void testControllerGetLog() throws Exception {
        when(logService.getLog((Integer)(log.getKodeLog()))).thenReturn(log);
        mvc.perform(get("/log/" + log.getKodeLog()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.kodeLog").value(log.getKodeLog()));
    }

    @Test
    public void testControllerGetNonExistLog() throws Exception{
        mvc.perform(get("/log/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void testControllerUpdateLog() throws Exception {
        logService.createLog(log,mahasiswa);

        String descLog = "ADV125YIHA";
        log.setDescription(descLog);

        when(logService.updateLog(String.valueOf(log.getKodeLog()), log)).thenReturn(log);

        mvc.perform(put("/log/" + (log.getKodeLog()))
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapToJson(log)));
    }

    @Test
    void testControllerDeleteLog() throws Exception {
        Mahasiswa a = new Mahasiswa();
        a.setNpm("12345678");
        logService.createLog(log, mahasiswa);
        mvc.perform(delete("/log/" + log.getKodeLog()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

}