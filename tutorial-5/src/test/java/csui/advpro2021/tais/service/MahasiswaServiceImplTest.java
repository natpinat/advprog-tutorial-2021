package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.model.Report;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MahasiswaServiceImplTest {
    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @Mock
    private MataKuliahRepository mataKuliahRepository;

    @InjectMocks
    private MahasiswaServiceImpl mahasiswaService;

    private Mahasiswa mahasiswa;

    private Class<?> mahasiswaServiceClass;

    @BeforeEach
    public void setUp() throws ClassNotFoundException {
        mahasiswaServiceClass = Class.forName("csui.advpro2021.tais.service.MahasiswaServiceImpl");

        mahasiswa = new Mahasiswa();
        mahasiswa.setNpm("1906192052");
        mahasiswa.setNama("Maung Meong");
        mahasiswa.setEmail("maung@cs.ui.ac.id");
        mahasiswa.setIpk("4");
        mahasiswa.setNoTelp("081317691718");
    }

    @Test
    public void testServiceCreateMahasiswa(){
        lenient().when(mahasiswaService.createMahasiswa(mahasiswa)).thenReturn(mahasiswa);
    }

    @Test
    public void testServiceGetListMahasiswa(){
        Iterable<Mahasiswa> listMahasiswa = mahasiswaRepository.findAll();
        lenient().when(mahasiswaService.getListMahasiswa()).thenReturn(listMahasiswa);
        Iterable<Mahasiswa> listMahasiswaResult = mahasiswaService.getListMahasiswa();
        Assertions.assertIterableEquals(listMahasiswa, listMahasiswaResult);
    }

    @Test
    public void testServiceGetMahasiswaByNpm(){
        lenient().when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);
        Mahasiswa resultMahasiswa = mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm());
        assertEquals(mahasiswa.getNpm(), resultMahasiswa.getNpm());
    }

    @Test
    public void testServiceDeleteMahasiswa(){
        mahasiswaService.createMahasiswa(mahasiswa);
        mahasiswaService.deleteMahasiswaByNPM("1111");
        lenient().when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);
        assertEquals(mahasiswa, mahasiswaService.getMahasiswaByNPM("1906192052"));
        mahasiswaService.deleteMahasiswaByNPM("1906192052");
        lenient().when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(null);
        assertEquals(null, mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm()));
        MataKuliah matkul = new MataKuliah("A1","Adpro","Ilkom");
        mahasiswa.updateMatkul(matkul);
        lenient().when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);



    }

    @Test
    public void testServiceUpdateMahasiswa(){
        mahasiswaService.createMahasiswa(mahasiswa);
        String currentIpkValue = mahasiswa.getIpk();
        //Change IPK from 4 to 3
        mahasiswa.setIpk("3");

        lenient().when(mahasiswaService.updateMahasiswa(mahasiswa.getNpm(), mahasiswa)).thenReturn(mahasiswa);
        Mahasiswa resultMahasiswa = mahasiswaService.updateMahasiswa(mahasiswa.getNpm(), mahasiswa);

        assertNotEquals(resultMahasiswa.getIpk(), currentIpkValue);
        assertEquals(resultMahasiswa.getNama(), mahasiswa.getNama());
    }

    @Test
    public void testCreateSummary() throws Exception {
        mahasiswaService.createMahasiswa(mahasiswa);
        Class<?>[] hm = new Class[1];
        hm[0] = Mahasiswa.class;
        List<Report> reports = new ArrayList<>();
        lenient().when(mahasiswaService.createSummaryLog(mahasiswa)).thenReturn(reports);

        Log log = new Log();
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime a = LocalDateTime.parse("2020-03-04 12:00:00",df);
        LocalDateTime b = LocalDateTime.parse("2020-03-04 14:00:00",df);
        log.setKodeLog(1);
        log.setStartTime(a);
        log.setEndTime(b);
        log.setDescription("desc");

        MataKuliah matkul = new MataKuliah("A1","Adpro","Ilkom");

        mahasiswa.updateMatkul(matkul);
        mahasiswa.addLog(log);
        mahasiswaService.createSummaryLog(mahasiswa);


        Method createSum = mahasiswaServiceClass.getDeclaredMethod("createSummaryLog",hm);
        assertEquals("java.util.List<csui.advpro2021.tais.model.Report>",
                createSum.getGenericReturnType().getTypeName());


    }
    @Test
    public void testCreateAsdos() throws Exception {
        MataKuliah matkul = new MataKuliah();
        matkul.setKodeMatkul("A1");
        matkul.setNama("bb");
        matkul.setProdi("aa");
        lenient().when(mahasiswaService.createAsdos(mahasiswa, matkul)).thenReturn(mahasiswa);
        assertEquals(matkul, mahasiswa.getMatakuliah());
        Class<?>[] hm = new Class[2];
        hm[0] = Mahasiswa.class;
        hm[1] = MataKuliah.class;
        Method createSum = mahasiswaServiceClass.getDeclaredMethod("createAsdos",hm);
        assertEquals("csui.advpro2021.tais.model.Mahasiswa",
                createSum.getGenericReturnType().getTypeName());

    }
    @Test
    public void testFindMataKuliah(){
        MataKuliah matkul = new MataKuliah("A1","Adpro","Ilkom");

        mataKuliahRepository.save(matkul);
        lenient().when(mahasiswaService.findMataKuliah("A1")).thenReturn(matkul);
    }

}
