package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class LogServiceImplTest {
    @Mock
    private LogRepository logRepository;

    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @InjectMocks
    private LogServiceImpl logService;

    private Log log;
    private Mahasiswa mahasiswa;

    @BeforeEach
    public void setUp(){
        mahasiswa = new Mahasiswa("190929390","haha","adfj","1","0987345");
        mahasiswaRepository.save(mahasiswa);
        log = new Log();
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime a = LocalDateTime.parse("2020-03-04 12:00:00",df);
        LocalDateTime b = LocalDateTime.parse("2020-03-04 14:00:00",df);
        log.setKodeLog(1);
        log.setStartTime(a);
        log.setEndTime(b);
        log.setDescription("desc");
        log.setMahasiswa(mahasiswa);
    }

    @Test
    public void testServiceCreateLog(){
        lenient().when(logService.createLog(log,mahasiswa)).thenReturn(log);
        assertEquals(1, log.getKodeLog());
    }

    @Test
    public void testServiceGetListLog(){
        Iterable<Log> listLog = logRepository.findAll();
        lenient().when(logService.getListLog()).thenReturn(listLog);
        Iterable<Log> listLogResult = logService.getListLog();
        Assertions.assertIterableEquals(listLog, listLogResult);
    }

    @Test
    public void testServiceGetLog(){
        logService.createLog(log, mahasiswa);
        lenient().when(logService.getLog(1)).thenReturn(log);
        Log resultLog = logService.getLog(log.getKodeLog());
        assertEquals(log.getKodeLog(), resultLog.getKodeLog());
    }

    @Test
    public void testServiceDeleteLog(){
        logService.createLog(log, mahasiswa);
        logService.deleteLog(1);
        lenient().when(logService.getLog(1)).thenReturn(null);
        assertEquals(null, logService.getLog(log.getKodeLog()));
    }

    @Test
    public void testServiceUpdateLog(){
        logService.createLog(log, mahasiswa);
        String currentDescValue = log.getDescription();
        log.setDescription("3");

        lenient().when(logService.updateLog(String.valueOf(log.getKodeLog()), log)).thenReturn(log);
        Log resultLog = logService.updateLog(String.valueOf(log.getKodeLog()), log);

        assertNotEquals(resultLog.getDescription(), currentDescValue);
        assertEquals(resultLog.getDescription(), log.getDescription());


    }

}
