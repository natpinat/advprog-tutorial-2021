package csui.advpro2021.tais.model;

import csui.advpro2021.tais.service.MataKuliahServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MataKuliahTest {

    private MataKuliahServiceImpl mataKuliahService;
    private Class<?>matkulClass;
    private MataKuliah matkul;

    @BeforeEach
    public void setup() throws Exception {
        matkulClass = Class.forName("csui.advpro2021.tais.model.MataKuliah");
    }

    @Test
    public void testCreateMataKuliah(){
        String kode = "12";
        String nama = "test";
        String prodi = "ilkom";
        matkul = new MataKuliah(kode, nama, prodi);
    }


}