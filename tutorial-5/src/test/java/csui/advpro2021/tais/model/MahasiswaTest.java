package csui.advpro2021.tais.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MahasiswaTest {

    private Mahasiswa mahasiswa;
    private Class<?> mahasiswaClass;

    @BeforeEach
    public void setup() throws Exception {
        mahasiswaClass = Class.forName("csui.advpro2021.tais.model.Mahasiswa");
    }

    @BeforeEach
    public void testCreateMahasiswa(){
        String npm = "12345678";
        String nama = "Pinat";
        String email = "tarta@tarta";
        String ipk = "3.3";
        String notelp = "213456712";
        mahasiswa = new Mahasiswa(npm,nama,email,ipk,notelp);
    }

    @Test
    public void testUpdateMatkul() throws Exception {
        Class<?>[] matkul = new Class[1];
        matkul[0] = MataKuliah.class;
        Method b = mahasiswaClass.getDeclaredMethod("updateMatkul",matkul);
        int methodModifiers = b.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, b.getParameterCount());
    }

}
