package csui.advpro2021.tais.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ReportTest {
    private Report report;
    private Class<?> reportClass;

    @BeforeEach
    public void setup() throws Exception {
        reportClass = Class.forName("csui.advpro2021.tais.model.Report");
    }

    @BeforeEach
    public void testCreateReport(){
        String month = "month";
        double jam = 4;
        int pay = 100000;
        report = new Report(month,jam, pay);
    }
    @Test
    public void testSetGetMonthReport() throws Exception {
        Class<?>[] mon = new Class[1];
        mon[0] = String.class;
        Method setMonth = reportClass.getDeclaredMethod("setMonth",mon);
        int methodModifiers = setMonth.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, setMonth.getParameterCount());

        report.setMonth("jan");
        assertEquals("jan",report.getMonth());
    }
    @Test
    public void testSetGetJamReport() throws Exception {
        Class<?>[] jam = new Class[1];
        jam[0] = double.class;
        Method setJam = reportClass.getDeclaredMethod("setTimeSpent",jam);
        int methodModifiers = setJam.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, setJam.getParameterCount());

        report.setTimeSpent(2.0);
        assertEquals(2.0,report.getTimeSpent());
    }
    @Test
    public void testSetGetPaymentReport() throws Exception {
        Class<?>[] pay = new Class[1];
        pay[0] = int.class;
        Method setPay = reportClass.getDeclaredMethod("setPayment",pay);
        int methodModifiers = setPay.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, setPay.getParameterCount());

        report.setPayment(1000);
        assertEquals(1000,report.getPayment());
    }

}
