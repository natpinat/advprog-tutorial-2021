package csui.advpro2021.tais.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LogTest {
    private Log log;
    private Class<?> logClass;

    @BeforeEach
    public void setup() throws Exception {
        logClass = Class.forName("csui.advpro2021.tais.model.Log");
    }

    @Test
    public void testCreateLog(){
        String start = "2020-03-04 12:00:00";
        String end = "2020-03-04 13:00:00";
        String desc = "sed";
        log = new Log(start,end,desc);
    }

}


