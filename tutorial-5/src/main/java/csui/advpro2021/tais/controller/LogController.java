package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/log")
public class LogController {
    @Autowired
    private LogService logService;

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Log>> getListLog() {
        return ResponseEntity.ok(logService.getListLog());
    }

    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity createLog(@RequestBody Log log,@RequestParam String npm) {
        Mahasiswa mahasiswa = logService.findMahasiswa(npm);
        return ResponseEntity.ok(logService.createLog(log,mahasiswa));
    }

    @GetMapping(path = "/{kodeLog}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getLog(@PathVariable(value = "kodeLog") Integer kodeLog) {
        Log log = logService.getLog(kodeLog);
        if (log == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(log);
    }

    @PutMapping(path = "/{kodeLog}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateLog(@PathVariable(value = "kodeLog") String kodeLog, @RequestBody Log log) {
        return ResponseEntity.ok(logService.updateLog(kodeLog, log));
    }

    @DeleteMapping(path = "/{kodeLog}", produces = {"application/json"})
    public ResponseEntity deleteLog(@PathVariable(value = "kodeLog") Integer kodeLog) {
        logService.deleteLog(kodeLog);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

}
