package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Entity
@Table(name = "log")
@Data
@NoArgsConstructor
public class Log {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "kode_log", updatable = false, nullable = false)
    private Integer kodeLog;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name = "start_time")
    private LocalDateTime startTime;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name = "end_time")
    private LocalDateTime endTime;

    @Column(name = "description")
    private String description;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "npm",nullable = false,updatable = false)
    private Mahasiswa mahasiswa;

    private float jamKerja;
    private int month;


    public Log(String start, String end, String description){
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime a = LocalDateTime.parse(start,df);
        LocalDateTime b = LocalDateTime.parse(end,df);
        this.startTime = a;
        this.endTime = b;
        this.description = description;

    }

}