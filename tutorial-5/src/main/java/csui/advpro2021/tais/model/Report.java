package csui.advpro2021.tais.model;

public class Report {
    private String month;

    private double timeSpent;

    private int payment;


    public Report( String month, double jam, int pay){
        this.month = month;
        this.payment = pay;
        this.timeSpent = jam;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getMonth() {
        return month;
    }

    public void setPayment(int payment) {
        this.payment = payment;
    }

    public int getPayment() {
        return payment;
    }

    public double getTimeSpent() {
        return timeSpent;
    }

    public void setTimeSpent(double timeSpent) {
        this.timeSpent = timeSpent;
    }
}