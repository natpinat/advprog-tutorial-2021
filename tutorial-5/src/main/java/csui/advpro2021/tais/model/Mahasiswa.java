package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "mahasiswa")
@Data
@NoArgsConstructor
public class Mahasiswa {

    @Id
    @Column(name = "npm", updatable = false)
    private String npm;

    @Column(name = "nama")
    private String nama;

    @Column(name = "email")
    private String email;

    @Column(name = "ipk")
    private String ipk;

    @Column(name = "no_telp")
    private String noTelp;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "kode_matkul")
    private MataKuliah matakuliah;

    @JsonIgnore
    @OneToMany(mappedBy = "mahasiswa")
    private List<Log> logList;

    public Mahasiswa(String npm, String nama, String email, String ipk, String noTelp) {
        this.npm = npm;
        this.nama = nama;
        this.email = email;
        this.ipk = ipk;
        this.noTelp = noTelp;
    }

    public void updateMatkul(MataKuliah matkul){
        this.matakuliah = matkul;
        this.logList = new ArrayList<>();
    }

    public void addLog(Log log){
        this.logList.add(log);
    }

}
