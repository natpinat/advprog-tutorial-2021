package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.model.Report;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MahasiswaServiceImpl implements MahasiswaService {
    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    private MataKuliahRepository mataKuliahRepository;

    @Autowired
    private LogRepository logRepository;



    @Override
    public Mahasiswa createMahasiswa(Mahasiswa mahasiswa) {
        mahasiswaRepository.save(mahasiswa);
        return mahasiswa;
    }

    @Override
    public MataKuliah findMataKuliah(String kodeMatkul){
        return mataKuliahRepository.findByKodeMatkul(kodeMatkul);
    }


    @Override
    public Mahasiswa createAsdos(Mahasiswa mahasiswa, MataKuliah matakuliah){
        //MataKuliah b = mataKuliahRepository.findByKodeMatkul(kodeMatkul);
        if (mahasiswa!= null){
            mahasiswa.updateMatkul(matakuliah);
            mahasiswaRepository.save(mahasiswa);
        }
        return null;
    }

    @Override
    public Iterable<Mahasiswa> getListMahasiswa() {
        return mahasiswaRepository.findAll();
    }

    @Override
    public Mahasiswa getMahasiswaByNPM(String npm) {
        return mahasiswaRepository.findByNpm(npm);
    }

    @Override
    public Mahasiswa updateMahasiswa(String npm, Mahasiswa mahasiswa) {
        mahasiswa.setNpm(npm);
        mahasiswaRepository.save(mahasiswa);
        return mahasiswa;
    }

    @Override
    public void deleteMahasiswaByNPM(String npm) {
        if(this.getMahasiswaByNPM(npm) != null) {
            if(this.getMahasiswaByNPM(npm).getMatakuliah() != null){
                return;
            }
        }
        else {
            mahasiswaRepository.deleteById(npm);
        }
    }

    @Override
    public List<Report> createSummaryLog(Mahasiswa mahasiswa){
        List<Report> listOutput = new ArrayList<>();
        if (mahasiswa != null) {
            String[] months = {"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli",
                    "Agustus", "September", "Oktober", "November", "Desember"};

            Map<String, Float> map = new HashMap<>();
            for (int i = 1; i <= 12; i++) {
                map.put(months[i - 1], (float) 0);
            }
            if (mahasiswa.getLogList()!=null) {
                for (Log g : mahasiswa.getLogList()) {
                    if (g == null) {
                        break;
                    }
                    float hour = g.getJamKerja();
                    if(g.getMonth()==0){
                        continue;
                    }
                    float h2 = map.get(months[g.getMonth() - 1]);
                    map.put(months[g.getMonth() - 1], hour + h2);
                }
            }
            for (int i = 1; i <= 12; i++) {
                float hours = map.get(months[i - 1]);
                Report n = new Report(months[i - 1], (double) hours, (int) (hours * 350));
                listOutput.add(n);
            }
        }
        return listOutput;
    }

}
