package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;

public interface LogService {
    Iterable<Log> getListLog();

    Mahasiswa findMahasiswa(String npm);
    Log createLog(Log log, Mahasiswa mahasiswa);

    Log getLog(Integer kodeMatkul);

    Log updateLog(String kodeMatkul, Log log);

    void deleteLog(Integer kodeMatkul);
}
