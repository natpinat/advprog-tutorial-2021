package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.model.Report;

import java.util.List;

public interface MahasiswaService {
    Mahasiswa createMahasiswa(Mahasiswa mahasiswa);

    MataKuliah findMataKuliah(String kodeMatkul);
    Mahasiswa createAsdos(Mahasiswa mahasiswa, MataKuliah mataKuliah);

    Iterable<Mahasiswa> getListMahasiswa();

    Mahasiswa getMahasiswaByNPM(String npm);

    Mahasiswa updateMahasiswa(String npm, Mahasiswa mahasiswa);

    void deleteMahasiswaByNPM(String npm);

    List<Report> createSummaryLog(Mahasiswa mahasiswa);


}
