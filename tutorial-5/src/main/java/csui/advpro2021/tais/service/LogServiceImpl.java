package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;

@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private LogRepository logRepository;

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Override
    public Iterable<Log> getListLog() {
        return logRepository.findAll();
    }

    @Override
    public Log getLog(Integer kodelog) {
        return logRepository.findByKodeLog(kodelog);
    }
    @Override
    public Mahasiswa findMahasiswa(String npm){
        return mahasiswaRepository.findByNpm(npm);
    }

    @Override
    public Log createLog(Log log, Mahasiswa mahasiswa) {
        Duration duration = Duration.between(log.getStartTime(),log.getEndTime());
        log.setJamKerja(duration.toHours());
        log.setMonth(log.getStartTime().getMonthValue());

        log.setMahasiswa(mahasiswa);
        logRepository.save(log);
        return log;
    }

   @Override
    public Log updateLog(String kodelog, Log log) {
        log.setKodeLog(Integer.parseInt(kodelog));
        logRepository.save(log);
        return log;
    }

    @Override
    public void deleteLog(Integer kodelog) {
        Log matkul = this.getLog(kodelog);
        logRepository.delete(matkul);
    }

}
