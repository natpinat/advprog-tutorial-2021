#ToDos dari Percakapan
1. Relationship one to many pada mata kuliah terhadap mahasiswa. Many to one pada mahasiswa terhadap mata kuliah
2. Mata kuliah yang sudah ada asisten tidak bisa dihapus, begitu pula dengan mahasiswa yang sudah menjadi asisten.
3. Membuat Object untuk parse ke json untuk summary
4. Mengimplementasikan Summary (biaya = 350/jam kerja)
5. Membuat Tabel Log untuk masing-masing mahasiswa. 1 mahasiswa bisa mempunyai banyak log. Mahasiswa tidak bisa mengisi 
   log bila bukan merupakan asdos
6. Mengganti password untuk postgres.
7. Selain membuat tabel log juga dibuat JPA Repositorynya, berserta service dan juga RestController. 