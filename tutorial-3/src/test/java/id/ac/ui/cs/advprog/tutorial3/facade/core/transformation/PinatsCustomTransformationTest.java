package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PinatsCustomTransformationTest {
    private Class<?> customClass;

    @BeforeEach
    public void setup() throws Exception {
        customClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.PinatsCustomTransformation");
    }

    @Test
    public void testPinatsCustomHasEncodeMethod() throws Exception {
        Method translate = customClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testPinatsCustomEncodesCorrectly() throws Exception {
        String text = "I wanna takoyaki";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "UL9mzzmL6mw1Amwu";

        Spell result = new PinatsCustomTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testPinatsCustomEncodesCorrectlyWithCustomKey() throws Exception {
        String text = "I wanna takoyaki";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "KBycppcBvcmq1cmk";

        Spell result = new PinatsCustomTransformation(2).encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testPinatsCustomHasDecodeMethod() throws Exception {
        Method translate = customClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testPinatsCustomDecodesCorrectly() throws Exception {
        String text = "UL9mzzmL6mw1Amwu";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "I wanna takoyaki";

        Spell result = new PinatsCustomTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testPinatsCustomDecodesCorrectlyWithCustomKey() throws Exception {
        String text = "KBycppcBvcmq1cmk";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "I wanna takoyaki";

        Spell result = new PinatsCustomTransformation(2).decode(spell);
        assertEquals(expected, result.getText());
    }
    
}

