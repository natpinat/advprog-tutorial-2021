package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
// halfway done
public class BowAdapter implements Weapon {
    private Bow bow;
    private boolean isAimShot;

    public BowAdapter(Bow bow){
        this.bow = bow;
        this.isAimShot = false;
    }

    @Override
    public String normalAttack() {
        return this.bow.shootArrow(this.isAimShot);
    }

    @Override
    public String chargedAttack() {
        // check if in aim shot mode or not
        if(this.isAimShot == true){
            this.isAimShot = false;
            return "Leaving charge mode";
        }
        else if(this.isAimShot == false){
            this.isAimShot = true;
            return "Entering charge mode";
        }
        return null;
    }

    @Override
    public String getName() {
        return this.bow.getName();
    }

    @Override
    public String getHolderName() {
        // TODO: complete me
        return this.bow.getHolderName();
    }
}
