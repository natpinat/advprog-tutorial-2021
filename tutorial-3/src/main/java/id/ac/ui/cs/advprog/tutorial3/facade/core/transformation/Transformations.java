package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

// compile all transformation only (additional facade)
public class Transformations {
    protected AbyssalTransformation abyss = new AbyssalTransformation();
    protected CelestialTransformation celes = new CelestialTransformation();
    protected PinatsCustomTransformation pinat = new PinatsCustomTransformation();

    public Transformations(){}

    // encode order custom -> celestial -> abyss
    public Spell encode(Spell spell) {
        Spell one = pinat.encode(spell);
        Spell two = celes.encode(one);
        Spell three = abyss.encode(two);
        Spell translated = CodexTranslator.translate(three, RunicCodex.getInstance());
        return translated;
    }
    // decode order abyss -> celestial -> custom
    public Spell decode(Spell spell) {
        Spell one = abyss.decode(spell);
        Spell two = celes.decode(one);
        Spell three = pinat.decode(two);
        Spell translated = CodexTranslator.translate(three, AlphaCodex.getInstance());
        return translated;
    }


}
