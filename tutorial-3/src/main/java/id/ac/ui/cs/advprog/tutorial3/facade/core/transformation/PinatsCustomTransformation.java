package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

// custom transformation using caesar cryptography
public class PinatsCustomTransformation{
    private int key;

    public PinatsCustomTransformation(int key){
        this.key = key;
    }

    public PinatsCustomTransformation(){
        this(12);
    }

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    // based on index of characters in each codex characters
    // for encode shift right key times, decode shift left key times
    private Spell process(Spell spell, boolean encode){
        int selector = encode ? 1 : -1;
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int codexSize = codex.getCharSize();
        char[] res = new char[text.length()];
        for(int i = 0; i < res.length; i++){
            char cur = text.charAt(i);
            int index = codex.getIndex(cur);
            if(selector > 0){
                // if more than list size then continue from beginning
                if(index+key > codexSize-1){
                    index = key - (codexSize-1-index);
                }
                else {
                    index+=key;
                }
            }
            else {
                // if less than 0 then continue from the end
                if(index-key < 0){
                    index = (codexSize-1)-(key-index);
                }
                else {
                    index-=key;
                }
            }
            res[i] = codex.getChar(index);
        }

        return new Spell(new String(res), codex);
    }
}
