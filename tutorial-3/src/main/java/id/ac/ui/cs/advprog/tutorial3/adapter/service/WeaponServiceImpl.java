package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private WeaponRepository weaponRepository;
    @Autowired
    private BowRepository bowRepository;
    @Autowired
    private SpellbookRepository spellbookRepository;

    private List<Weapon> combi = new ArrayList<>();

    // TODO: implement me
    // add all weapons into a single list for view
    @Override
    public List<Weapon> findAll() {
        if (combi.isEmpty() == true) {
            for (Weapon weapon: weaponRepository.findAll()) {
                combi.add(weapon);
            }
            for (Bow bow: bowRepository.findAll()) {
                combi.add(new BowAdapter(bow));
            }
            for (Spellbook book : spellbookRepository.findAll()) {
                combi.add(new SpellbookAdapter(book));
            }
        }
        return combi;
    }


    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon temp = this.weaponRepository.findByAlias(weaponName);
        // if not available then check bow first
        System.out.println(temp == null);
        if ((this.spellbookRepository.findByAlias(weaponName)) != null) {
            temp = new SpellbookAdapter(this.spellbookRepository.findByAlias(weaponName));

        }
        if (this.bowRepository.findByAlias(weaponName) != null) {
            temp = new BowAdapter(this.bowRepository.findByAlias(weaponName));

        }
        // if not available again then check spellbook

        weaponRepository.save(temp);
        // 1 for normal attack, 2 for charged attack
        // make a string format then added to log
        if (attackType == 1) {
            String temporary = temp.normalAttack();
            if (temporary!= null){
                String inputLog = String.format("%s attacked with %s (normal attack) : %s",temp.getHolderName(),temp.getName(),temporary);
                logRepository.addLog(inputLog);
            }
        }
        else if (attackType == 2){
            String temporary2 = temp.chargedAttack();
            if (temporary2!= null) {
                String inputLog = String.format("%s attacked with %s (charged attack) : %s", temp.getHolderName(), temp.getName(), temporary2);
                logRepository.addLog(inputLog);
            }
        }
    }

    // TODO: implement me
    // return all logs
    @Override
    public List<String> getAllLogs() {
        return this.logRepository.findAll();
    }
}
