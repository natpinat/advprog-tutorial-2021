package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;

public class HighSpiritAttackSpell extends HighSpiritSpell {
    // TODO: Complete Me
    // done

    public HighSpiritAttackSpell(HighSpirit spirit){
        super(spirit);
    }

    @Override
    public void cast() {
        this.spirit.attackStance();
    }

    @Override
    public void undo() {
        super.undo();
    }

    @Override
    public String spellName() {
        return spirit.getRace() + ":Attack";
    }
}
