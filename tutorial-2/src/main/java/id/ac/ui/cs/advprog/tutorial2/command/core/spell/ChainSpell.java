package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import org.springframework.boot.autoconfigure.web.ResourceProperties;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    // done
    ArrayList<Spell>spells;

    public ChainSpell(ArrayList<Spell>spells) {
        this.spells = spells;
    }

    @Override
    public void cast() {
        // cast from first to last
        for (int i = 0 ; i < spells.size() ; i++){
            spells.get(i).cast();
        }
    }

    @Override
    // undo the from last to first
    public void undo() {
        for (int i = 0 ; i < spells.size() ; i++) {
            spells.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
